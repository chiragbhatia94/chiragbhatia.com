'use strict';

// Initializes Portfolio.
function Portfolio() {
    this.checkSetup();

    this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
Portfolio.prototype.initFirebase = function () {

    // Shortcuts to DOM Elements
    this.contactForm = document.getElementById('contact-form');
    this.firstname = document.getElementById('firstname');
    this.surname = document.getElementById('surname');
    this.email = document.getElementById('email');
    this.message = document.getElementById('message');
    this.submitButton = document.getElementById('submit');

    this.successMessage = document.getElementById('successMessage');
    // this.successMessage.style.visibility = 'hidden';

    // Save message to the database.
    this.contactForm.addEventListener('submit', this.saveMessage.bind(this));

    // Shortcuts to Firebase SDK features
    this.database = firebase.database();
};

Portfolio.prototype.saveMessage = function (e) {
    e.preventDefault();

    if (this.message.value) {
        // console.log("wow " + firstname.value + " " + surname.value + " " + message.value);

        this.messageRef = this.database.ref('messages');
        this.messageRef.push({
            firstname: firstname.value,
            surname: surname.value,
            email: email.value,
            message: message.value
        }).then(function () {
            document.getElementById('contact-form').reset();
            successMessage.style.visibility = 'visible';
            setTimeout(function () {
                successMessage.style.visibility = 'hidden';
            }, 10000);
        });
    }
};

// Checks that the Firebase SDK has been correctly setup and configured.
Portfolio.prototype.checkSetup = function () {
    if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
            'Make sure you go through the codelab setup instructions and make ' +
            'sure you are running the codelab using `firebase serve`');
    }
};

window.onload = function () {
    window.friendlyChat = new Portfolio();
};
