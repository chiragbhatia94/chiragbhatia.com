'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');

const gmailEmail = encodeURIComponent(functions.config().gmail.email);
const gmailPassword = encodeURIComponent(functions.config().gmail.password);
const mailTransport = nodemailer.createTransport(
    `smtps://${gmailEmail}:${gmailPassword}@smtp.gmail.com`);

exports.sendMailOnMessage = functions.database.ref('/messages/{pushId}')
    .onWrite(event => {
        const data = event.data.val();
        console.log('Sending mail', event.params.pushId, data);

        const mailOptions = {
            from: '"chiragbhatia.com" <noreply@firebase.com>',
            to: "chiragbhatia94@gmail.com"
        };

        mailOptions.subject = data.firstname + " " + data.surname +
            " tried to contact you on chiragbhatia.com.";
        mailOptions.text = "From: " + data.email + ".\n" + data.message;

        return mailTransport.sendMail(mailOptions).then(() => {
            console.log('Mail sent to: ', 'chiragbhatia94@gmail.com');
        })
    });
